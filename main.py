from lexer import Lexer
from Parser import Parser


fileName = input("Enter File Name:")
# fileName = "test2.txt"
file = open(fileName)
text_input = file.read()
file.close()

lexer = Lexer().build()
lexer.input(text_input)

parser = Parser()
parser.build().parse(text_input, lexer, False)


# text_file = open("Output_"+fileName, "w")
# while True:
#     tok = lexer.token()
#     if not tok: break
#     print(tok.type, end=" ")
    # text_file.write(tok.type+" ")
# text_file.close()
