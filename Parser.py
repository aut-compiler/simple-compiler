from ply import yacc
from lexer import Lexer


class Parser:
    tokens = Lexer().tokens

    def __init__(self):
        pass

    def p_program(self, p):
        """ program : declist MAIN LRB RRB block"""
        print(" program : declist MAIN LRB RRB block")

    def p_declist1(self, p):
        """declist : dec declist"""
        print(" declist : dec declist")

    def p_declist2(self, p):
        """ declist : """
        print(" declist : ")

    def p_dec1(self, p):
        """ dec : vardec"""
        print(" dec : vardec")

    def p_dec2(self, p):
        """dec : funcdec"""
        print(" dec : funcdec")

    def p_type1(self, p):
        """ type : INTEGER """
        print(" type : INTEGER ")

    def p_type2(self, p):
        """ type : FLOAT """
        print(" type : FLOAT ")

    def p_type3(self, p):
        """ type : BOOLEAN """
        print(" type : BOOLEAN ")

    def p_iddec1(self, p):
        """ iddec : ID """
        print(" iddec : ID ")

    def p_iddec2(self, p):
        """ iddec : ID LSB exp RSB """
        print(" iddec : ID LSB exp RSB ")

    def p_iddec3(self, p):
        """ iddec : ID ASSIGN exp """
        print(" iddec : ID ASSIGN exp ")

    def p_idlist1(self, p):
        """ idlist : iddec """
        print(" idlist : iddec ")

    def p_idlist2(self, p):
        """ idlist : idlist COMMA iddec """
        print(" idlist : idlist COMMA iddec ")

    def p_vardec(self, p):
        """ vardec : type idlist SEMICOLON"""
        print(" vardec : type idlist SEMICOLON")

    def p_funcdec1(self, p):
        """ funcdec : type ID LRB paramdecs RRB block """
        print(" funcdec : type ID LRB paramdecs RRB block ")

    def p_funcdec2(self, p):
        """ funcdec : VOID ID LRB paramdecs RRB block """
        print(" funcdec : VOID ID LRB paramdecs RRB block ")


    def p_paramdecs1(self, p):
        """ paramdecs : paramdecslist """
        print(" paramdecs : paramdecslist ")

    def p_paramdecs2(self, p):
        """ paramdecs : """
        print(" paramdecs : ")

    def p_paramdecslist1(self, p):
        """ paramdecslist : paramdec """
        print(" paramdecslist : paramdec ")

    def p_paramdecslist2(self, p):
        """ paramdecslist : paramdec COMMA paramdecslist"""
        print(" paramdecslist : paramdec COMMA paramdecslist")

    def p_paramdec1(self, p):
        """ paramdec : type ID """
        print(" paramdec : type ID ")

    def p_paramdec2(self, p):
        """ paramdec : type ID LSB RSB """
        print(" paramdec : type ID LSB RSB ")

    def p_varlist1(self, p):
        """ varlist : vardec varlist """
        print(" varlist : vardec varlist ")

    def p_varlist2(self, p):
        """ varlist : """
        print(" varlist : ")


    def p_block(self, p):
        """ block : LCB varlist stmlist RCB"""
        print(" block : LCB varlist stmlist RCB")

    def p_stmlist1(self, p):
        """ stmlist : stmt stmlist """
        print(" stmlist : stmt stmlist ")

    def p_stmlist2(self, p):
        """ stmlist : """
        print(" stmlist : ")

    def p_lvalue1(self, p):
        """ lvalue : ID """
        print(" lvalue : ID ")

    def p_lvalue2(self, p):
        """ lvalue : ID LSB exp RSB"""
        print(" lvalue : ID LSB exp RSB")

    def p_stmt1(self, p):
        """ stmt : RETURN exp SEMICOLON """
        print(" stmt : RETURN exp SEMICOLON ")

    def p_stmt2(self, p):
        """ stmt : exp SEMICOLON """
        print(" stmt : exp SEMICOLON ")

    def p_stmt3(self, p):
        """ stmt : block """
        print(" stmt : block ")

    def p_stmt4(self, p):
        """ stmt : WHILE LRB exp RRB stmt"""
        print(" stmt : WHILE LRB exp RRB stmt")

    def p_stmt5(self, p):
        """ stmt : FOR LRB exp SEMICOLON exp SEMICOLON exp RRB stmt"""
        print(" stmt : FOR LRB exp SEMICOLON exp SEMICOLON exp RRB stmt")

    def p_stmt6(self, p):
        """ stmt : IF LRB exp RRB stmt elseiflist %prec NEXTIF """
        print(" stmt : IF LRB exp RRB stmt elseiflist %prec NEXTIF ")

    def p_stmt7(self, p):
        """ stmt : IF LRB exp RRB stmt elseiflist ELSE stmt """
        print(" stmt : IF LRB exp RRB stmt elseiflist ELSE stmt ")

    def p_stmt8(self, p):
        """ stmt : PRINT LRB ID RRB SEMICOLON"""
        print(" stmt : PRINT LRB ID RRB SEMICOLON")

    def p_elseiflist1(self, p):
        """ elseiflist : ELIF LRB exp RRB stmt elseiflist """
        print(" elseiflist : ELIF LRB exp RRB stmt elseiflist ")

    def p_elseiflist2(self, p):
        """ elseiflist : %prec NEXTIF """
        print(" elseiflist : %prec NEXTIF ")

    def p_exp1(self, p):
        """ exp : lvalue ASSIGN exp """
        print(" exp : lvalue ASSIGN exp ")

    def p_exp2_or(self, p):
        """ exp : exp OR exp """
        print(" exp : exp OR exp ")

    def p_exp2_and(self, p):
        """ exp : exp AND exp """
        print(" exp : exp AND exp ")

    def p_exp2_sum(self, p):
        """ exp : exp SUM exp"""
        print(" exp : exp SUM exp")

    def p_exp_sub(self, p):
        """ exp : exp SUB exp """
        print(" exp : exp SUB exp ")

    def p_exp2_mul(self, p):
        """ exp : exp MUL exp """
        print(" exp : exp MUL exp ")

    def p_exp2_mod(self, p):
        """ exp : exp MOD exp """
        print(" exp : exp MOD exp ")

    def p_exp2_div(self, p):
        """ exp : exp DIV exp"""
        print(" exp : exp DIV exp")

    def p_exp3_ge(self, p):
        """ exp : exp GE exp """
        print(" exp : exp GE exp ")

    def p_exp3_le(self, p):
        """ exp : exp LE exp """
        print(" exp : exp LE exp ")

    def p_exp3_eq(self, p):
        """ exp : exp EQ exp """
        print(" exp : exp EQ exp")

    def p_exp3_ne(self, p):
        """ exp : exp NE exp """
        print(" exp : exp NE exp ")

    def p_exp3_lt(self, p):
        """ exp : exp LT exp """
        print(" exp : exp LT exp ")

    def p_exp3_gt(self, p):
        """ exp : exp GT exp """
        print(" exp : exp GT exp ")

    def p_exp4(self, p):
        """ exp : const """
        print(" exp : const ")

    def p_exp5(self, p):
        """ exp : lvalue """
        print(" exp : lvalue ")

    def p_exp6(self, p):
        """ exp : ID LRB explist RRB """
        print(" exp : ID LRB explist RRB ")

    def p_exp7(self, p):
        """ exp : LRB exp RRB """
        print(" exp : LRB exp RRB ")

    def p_exp8(self, p):
        """ exp : ID LRB RRB """
        print(" exp : ID LRB RRB ")

    def p_exp9(self, p):
        """ exp : SUB exp %prec UNISUB"""
        print(" exp : SUB exp %prec UNISUB")

    def p_exp10(self, p):
        """ exp : NOT exp """
        print(" exp : NOT exp ")

    def p_const_integernumber(self, p):
        """ const : INTEGERNUMBER """
        print(" const : INTEGERNUMBER ")

    def p_const_floatnumber(self, p):
        """ const : FLOATNUMBER """
        print(" const : FLOATNUMBER ")

    def p_const_true(self, p):
        """ const : TRUE """
        print(" const : TRUE ")

    def p_const_false(self, p):
        """ const : FALSE """
        pass

    def p_explist1(self, p):
        """ explist : exp """
        print(" explist : exp ")

    def p_explist2(self, p):
        """ explist : exp COMMA explist """
        print(" explist : exp COMMA explist ")

    precedence = (
        ('left', 'NEXTIF'),
        ('left', 'ELIF'),
        ('left', 'ELSE'),
        ('right', 'ASSIGN'),
        ('left', 'OR'),
        ('left', 'AND'),
        ('left', 'EQ', 'NE'),
        ('left', 'GE', 'LE', 'LT', 'GT'),
        ('left', 'SUM', 'SUB'),
        ('left', 'MUL', 'DIV', 'MOD'),
        ('left', 'NOT', 'UNISUB'),
        ('left', 'LRB', 'RRB', 'LSB', 'RSB'),

    )

    def p_error(self, p):
        raise Exception('PARSER ERROR : ', p)

    def build(self, **kwargs):
        """build the parser"""
        self.parser = yacc.yacc(module=self, **kwargs)
        return self.parser
