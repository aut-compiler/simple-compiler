from ply import lex


class Lexer:
    tokens = [
        'ID',
        'INTEGERNUMBER', 'FLOATNUMBER',
        'AND', 'OR', 'NOT',
        'ASSIGN', 'SUM', 'SUB', 'MUL', 'DIV', 'MOD',
        'GT', 'GE', 'LT', 'LE',
        'EQ', 'NE',
        'LCB', 'RCB', 'LRB', 'RRB', 'LSB', 'RSB',
        'SEMICOLON', 'COMMA',
        # 'RESERVED',
        'ERROR'
    ]

    reserved = {
        'int': 'INTEGER',
        'float': 'FLOAT',
        'bool': 'BOOLEAN',
        'void': 'VOID',
        'true': 'TRUE',
        'false': 'FALSE',
        'print': 'PRINT',
        'return': 'RETURN',
        'main': 'MAIN',
        'if': 'IF',
        'else': 'ELSE',
        'elif': 'ELIF',
        'while': 'WHILE',
        'for': 'FOR',
    }

    tokens += list(reserved.values())

    def t_RESERVED_ID(self, t):
        r'[a-zA-Z_][a-zA-Z0-9_]*'
        if t.value in Lexer.reserved.keys():
            t.type = Lexer.reserved.get(t.value)
            return t
        else:
            t.type = 'ID'
            return t

    def t_ERROR(self, t):
        r"""(\d+[a-zA-Z_][a-zA-Z0-9_]*)
             |((\+|-|%|\*|/)(\s*(\+|-|%|\*|/))+)
            |(\d\d\d\d\d\d\d\d\d\d+(\.)?(\d)*)
            |((\d)*(\.)\d\d\d\d\d\d\d\d\d\d+)
            |(\d*\.\d*(\.\d*)+)"""

        return t

    t_AND = r'&&'
    t_OR = r'\|\|'
    t_GE = r'>='
    t_LE = r'<='
    t_EQ = r'=='
    t_NE = r'!='

    t_NOT = r'!'
    t_ASSIGN = r'='
    t_GT = r'>'
    t_LT = r'<'

    t_SUM = r'\+'
    t_SUB = r'-'
    t_MUL = r'\*'
    t_DIV = r'/'
    t_MOD = r'\%'

    t_LCB = r'\{'
    t_RCB = r'\}'
    t_LRB = r'\('
    t_RRB = r'\)'
    t_LSB = r'\['
    t_RSB = r'\]'

    t_SEMICOLON = r';'
    t_COMMA = r','

    def t_FLOATNUMBER(self, t):
        r'\d\d?\d?\d?\d?\d?\d?\d?\d?\.\d\d?\d?\d?\d?\d?\d?\d?\d?'
        t.value = float(t.value)
        return t

    def t_INTEGERNUMBER(self, t):
        r'\d\d?\d?\d?\d?\d?\d?\d?\d?'
        t.value = int(t.value)
        return t

    def t_newline(self, t):
        r'\n+'
        t.lexer.lineno += len(t.value)

    t_ignore = '\n \t'

    def t_error(self, t):
        print ('\nIllegal character ', end='')
        print(t)
        t.lexer.skip(1)
        # raise Exception('ERROR : ',t.value)

    def build(self, **kwargs):
        self.lexer = lex.lex(module=self, **kwargs)
        return self.lexer
